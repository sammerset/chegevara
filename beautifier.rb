require 'optparse'

#its a test with small logfile, so will use RAM 
#as a storage

class ReqsStorage
  attr_accessor :storage, :regexes
  def initialize
    @storage = []
    @regexes = []
  end
end

module ReqsStatHelper
  class << self
    def to_ms(time_string)
      value, dimension = time_string.split(/(\D+)/)
      int_val = value.to_i
      return case dimension
        when "ms"
          int_val
        when "s"
          int_val*1000
        else
          int_val
      end
    end

    def beauty(val)
      return "none" if val.nil?
      val > 999 ? "#{val/1000}s": "#{val}ms"
    end 
    
    def moda_beauty(val)
      return "any mode with this request" if val.nil?
      val.map{|f| beauty(f)}.join(', ')
    end
 
    def endpoints_regexes
      rgxs = endpoints_regexes_cache 
      return rgxs if enps_cached?
      endpoints_collection.sort.each do |enp|
        rgxs << endpoint_regex(enp)
      end
      return rgxs
    end

    def endpoints_regexes_cache
      $store.regexes
    end

    def enps_cached?
      endpoints_regexes_cache.any?
    end

    def endpoints_collection
      [
        "GET /api/users/{user_id}/count_pending_messages",
        "GET /api/users/{user_id}/get_messages",
        "GET /api/users/{user_id}/get_friends_progress",
        "GET /api/users/{user_id}/get_friends_score",
        "POST /api/users/{user_id}",
        "GET /api/users/{user_id}"
      ]
    end

    def endpoint_regex(str)
      method, path = str.gsub("{user_id}", '\d+').split(/\s+/)
      return /.*\s+method=#{method}\s+path=#{path}\s+.*/
    end

    def endpoint_signs
      [
        /.*method=(GET|POST).*/,
        /.*path=\/api\/users\/.*/
      ]
    end

    def commonizer(str)
      str.gsub!(/\/\d+([^\d]*)/, '/{user_id}\1')
    end

    def break_loop?(line)
      return true if true == endpoint_signs.each do |regex|
        break(true) if line !~ regex
      end

      true != endpoints_regexes.each do |regex|
        break(true) if line =~ regex
      end
    end
  end
end

class Request
  attr_accessor :type, :url, :r_times, :dynos

  def initialize(type, url)
    @type, @url = type, url
    @r_times = []
    @dynos = []
  end
    
  def amounts
    @r_times.size
  end
  
  def mean(array=r_times)
    size = array.size
    size > 0 ? ((array.inject(0, :+))/size): 0
  end

  def median
    return nil if r_times.empty?
    arr = r_times.sort
    mid = arr.size/2
    arr.size % 2 == 1 ? arr[mid] : mean(arr[mid-1..mid])
  end

  def moda
    hsh = r_times.inject(Hash.new(0)) { |h, n| h[n] += 1; h }
    modes = nil
    hsh.each_pair do |item, times|
    modes << item if modes && times == modes[0]
    modes = [times, item] if 
                  (!modes && times>1) or (modes && times > modes[0])
    end
    return modes ? modes[1..modes.size] : modes
  end 
  
  def most_dyno
    @dynos.group_by { |e| e }.values.max_by { |values| values.size }.first
  end
  
  class << self
    def find_or_new(type, url)
      test_request = find(type, url)
      
      if test_request.nil?
        test_request = new(type, url)
        write_req(test_request)
      end
      
      return test_request
    end

    def absent
      ReqsStatHelper.endpoints_collection - all_type_url
    end

    def all
      get_source
    end
   
    private 
    
    def find(type, url)
      get_source.select do |f|  
        f.type == type && f.url == url
      end.first
    end
    
    def write_req(inst)
      $store.storage << inst
    end

    def get_source
      $store.storage
    end

    def all_type_url
      self.all.map{|f| "#{f.type} #{f.url}"}
    end
  end
end

module ReqsStat
  class << self
    def analize!(argvs)
      $store = ReqsStorage.new
      @hlp = ReqsStatHelper
      @options = {}
      opt_parse(argvs)
      parse_and_print_data
    end
    
    private

    def opt_parse(argvs)
      opt_parser = OptionParser.new do |opts|
        opts.banner = "Usage: beautifier.rb [-f]"
        opts.separator ""
        opts.on("-f", "--file [FILE]",
          "Need for setting log file source") do |file|
            @file_from = file
        end
      end
      opt_parser.parse!(argvs)
      abort "Missing option: --file" if @file_from.nil?
    end

    def parse_and_print_data
      parse_data!
      print_result
    end
    
    def parse_data!
      File.open(@file_from).each_line do |line|
        next if @hlp.break_loop?(line)
        @line = line
        @tr = Request.find_or_new(parser, @hlp.commonizer(parser("path")))
        @tr.r_times << (@hlp.to_ms(parser("connect")) + 
                                           @hlp.to_ms(parser("service")))
        @tr.dynos << parser("dyno")
      end
    end
    
    def print_result
      result = separator
      Request.all.sort_by{|f| f.amounts}.each do |tr|
        result << %(#{tr.type} #{tr.url} :::=>>> AMOUNTS: #{tr.amounts} |||| MEAN: #{@hlp.beauty(tr.mean)} |||| MEDIAN: #{@hlp.beauty(tr.median)} |||| MODA: #{@hlp.moda_beauty(tr.moda)} |||| MOST_DYNOS: #{tr.most_dyno}\n#{separator})
      end
      if Request.absent.any?
        result << "\nAbsent: \n#{Request.absent}\n#{separator}"
      end
      print(result)
    end

    def separator
      "\n------------------------------------------------------------\n\n"
    end
    
    def parser param="method"
      @line.gsub(/.*\s+#{param}=([^\s]+)\s+.*\n?/, '\1')
    end
  end 
end

ReqsStat.analize!(ARGV)
